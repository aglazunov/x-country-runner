# xRunner - Komoot coding challenge

[Live Demo](https://xrunner.glazunov.eu)

## Task statement

**Develop a react app that enables you to plan your cross country run and
download it as GPX file.**

[Cross Country](https://en.wikipedia.org/wiki/Cross_country_running) runners 
are not bound to the streets. Your users can plan their
favorite route across fields and hills by just placing markers as waypoints on
the map. For detailed planning the same waypoints show up as a list where
users can delete and rearrange them until the route is perfect and ready to
download. The user interface should be simple as shown on the left.

An excellent ReactJS Developer should manage to do this challenge in a few
hours. Impress us with your coding skills by not using 3rd party react
components and plugins. At least the map and the list should be separated
react components.

Send us the sources and what you have learned while developing

## Running the app

From the project folder, execute the following commands depending on your preferred package manager

- When using yarn:
    - `yarn`
    - `yarn start`
- When using npm
    - `npm install`
    - `npm start`

_*note_ further notes will be yarn-centric, the same would apply to npm with few changes.

## Tests

To run the jest-based unit test suite, execute `yarn test`. 
To run cypress-based integration/e2e test
- In GUI mode: `yarn run cypress open`   
- In headless mode: `yarn run cypress run`   

*note replace `yarn run` with `npx` when using npm.

## Structure

Main `App` component renders context provider (of `MapContext`) with a child `AppLayout`. `App` component utilizes
`useReducer` hook to access app-wide state and `dispatch` function. Some of the state values are provided as is to 
the context, others are used in memoized selectors to create data, that's ready to be consumed by react components.
Dispatch function is not provided to the context directly, but rather is used to create an action-dispatching functions,
which are passed down through context (similar to what redux connect would do with its `mapDispatchToProps`).

`AppLayout` represents the app's topmost graphical component and divides the screen into left panel area (with 
`ManagementPanel` inside) and map content area (with `PlanningMap`). It also reacts to content area resize by triggering
`onMapResize` event.

## User Guide

Click anywhere on the map to add a marker. Drag markers around with mouse to change their positions. Marker elements are
focusable with the keyboard (tab, shift + tab). Focused marker can be deleted with 'Delete' keyboard shortcut. Click on
a midpoint between two markers to add an intermediate marker between them. Manage the order of your waypoints from a 
left panel by dragging them around. You can also delete markers from the left panel. When you're done creating a route, 
press download GPX button to export it.   

## Stack and library limitations

Main limitation that I 've faced was not not to use additional libraries. This has resulted in some learnings but also 
in quick scope growth, as I needed to do a lot of ground work (like reimplementing redux, drag and drop, utility 
methods). I've based the app on create-react-app, and the only dependencies I've added were dev dependencies (eslint, 
prettier, cypress and prop-types, which is technically a peer dependency but historically part of react) to reinforce 
code quality.

# Functionality

I focused on providing a good UX (when interacting with waypoints and downloading maps) and DX (code quality, CI/CD). 
The main reason that people won't start using this app tomorrow is that the map starting point and zoom level are fixed
and there's no way for user to adjust it. Providing a smooth zoom experience for the map is a challenge that I've
left outside of scope of this app. So a very rare user would plan their cross-country run in exactly this area, 
especially considering the zoom level. Maybe some Slovakian giant/yeti :) Also, the app fully functional works on 
desktop browsers, but mobile support is limited.

## App state

App's main reducer is created with `combineReducers` (concept borrowed from redux, implemented from head, as well as
neighboring `createDispatcherMap`). It contains data with the following keys, each item reduced by a corresponding child 
reducer:
- `markers`: {array} list of marker's basic data
    - `x`: {number} x coordinate in pixels
    - `y`: {number} y coordinate in pixels
    - `id`: {number} unique marker identifier
- `mapLocation`: {object} parameters for the mapping engine
    - `startX`: {number} x of the top left map tile  
    - `startY`: {number} y of the top left tile
    - `z`: {number} map zoom level
    - `tileSize`: {number} tile size used by map provider 
    - `width`: {number} current map width in pixels
    - `width`: {number} current map height in pixels
- `ongoingInteractions`: {object} UI interactions happening at the moment (marker manipulation)  
  - `move`: for dragging the marker on the map
    - `id`: {string} id of the marker being moved (dragged on the map)
    - `point`: {array: [x, y]} relative (delta) position in pixels, to which the marker has been moved
  - `reorder`: for drag-n-drop interaction on the left panel
    - `srcIndex`: {number} index of the waypoint that's being reordered
    - `targetIndex`: {number} current (provisional) index, which the marker should take in case of completing the 
    drag-n-drop interaction 
 - `activeMarkerId` { string } – id of the marker that's focused (:focus) at the moment

## Components

All of them are implemented as functional react components. Some representational components utilize css module 
imports. Enforced by eslint rules, every used prop has a prop type declaration. Some logic used in the components 
is extracted to helper functions or to hooks. Most of the components that are accessing context have 
`ConnectedComponent` versions that encapsulate context access and use of _function as a child_ pattern.

## UX
To provide an enjoyable user experience, the following techniques were applied: 
- Each distinct state is reflected visually, so the changes become immediately visible to the user. Examples:
    - Focused state for markers on the map
    - Marker dragging state:
        - grab/grabbing cursors
        - marker and lines have distinct styles while dragging interaction is in process
    - When reordering markers interaction is in progress, display the tentative new connecting lines in the map
    - Both reordering and moving interactions use the same visual styles of map elements for similar concepts - 
    affected marker colored #dde25d and dashed tentative lines  
- Added midpoints between adjacent markers to simplify the process of adding and intermediate point
- Lines have double stroke to be visible on any background. Same goes for markers with fill/stroke
- Focused marker can be deleted with a keyboard shortcut 
- Interaction race cases are eliminated, so it's not possible for example to delete a marker while dragging it (these
handlers are detached when dragging or reordering interaction is in progress) 

## GPX Export

I had to educate myself on GPX format for this task. Turned out to be quite simple and manageable to create without an 
XML library. I learned that there are three main types of elements - waypoints, routes and tracks. I started by only 
exporting waypoints. Of course, I tested the exported files by importing to other software, mainly Google Maps. Turned 
out that the desired result (both waypoints as markers and the track as polyline) were produced when `<wpt>` elements 
were used together with `<trk>` or `<rte>`. As semantically route was a better choice then track, I went with using 
`<wpt>` and `<rte>`/`<rtept>` tags. And I made sure to get the idents right. Another discovery for me was ability to 
trigger a file download without a backend, copied from StackOverflow.

## Test suites

There are unit tests for some of the helper function and cypress test specs for adding , deleting and dragging markers.

## CI/CD

Gitlab CI/CD is set up to perform the following steps on each push master:
- check code with JS lint
- run unit tests
- // run cypress tests – commented out due to dependency issue (Error: spawn Xvfb ENOENT)
- run webpack build
- deploy to [https://xrunner.glazunov.eu](https://xrunner.glazunov.eu)

## Further development

### UX
- restrict dragging markers outside of the visible area
- auto scroll waypoint list when a new one is added
- auto focus on a next marker after the focused marker got deleted
- process drag-n-drop on WaypointList but outside of WaypointItem
- ability to drag the midpoint immediately
- keyboard events for interactions
    - reorder (focus on handle, press space to activate dnd, use arrows to move)
    - move focused marker (arrows with shift/alt modifiers for different movement speed)
    - move map (ability to focus on the map and use the arrow keys to move)
    - focus on midpoint, enter to create a marker
    - ability to cancel interactions on escape

### Functionality
- allow panning and zooming
- mobile support: collapsible panel, component for manipulating focused marker (delete button, reorder arrows)

### DX
- adding integration tests for left panel
- adding integration tests for left panel and map integration
- check helper/util functions placement imports (e.g. move tile2lat and tile2long methods)
- useElementSize and useWindowSize could be using the same window event handler 

### Outside of challenge scope
- import GPX
- zoom to fit the track
- change map provider
- persist the track between page visits
- link to track
