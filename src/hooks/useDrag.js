import { useState, useEffect } from 'react';
import { isTouchSupported } from './helpers';

const useDrag = (ref, onDragStart, onDragMove, onDragEnd) => {
  const [dragPoint, setDragPoint] = useState(null);
  const [startPoint, setStartPoint] = useState(null);

  useEffect(() => {
    const handleMouseDown = (event) => {
      const target = event.touches ? event.touches[0] : event;
      setStartPoint([target.clientX, target.clientY]);
      setDragPoint([0, 0]);
      onDragStart && onDragStart(target.pageX, target.pageY);
    };

    const refValue = ref.current;
    const touchEventsAttached = isTouchSupported();

    refValue.addEventListener('mousedown', handleMouseDown);
    touchEventsAttached &&
      refValue.addEventListener('touchstart', handleMouseDown);

    return () => {
      refValue.removeEventListener('mousedown', handleMouseDown);
      touchEventsAttached &&
        refValue.removeEventListener('touchstart', handleMouseDown);
    };
  }, [onDragStart, ref, setDragPoint, setStartPoint]);

  useEffect(() => {
    const handleMouseUp = () => {
      setStartPoint(null);
      setDragPoint(null);
      onDragEnd && onDragEnd(...dragPoint);
    };

    const handleMouseMove = (event) => {
      const target = event.touches ? event.touches[0] : event;

      const x = target.clientX - startPoint[0];
      const y = target.clientY - startPoint[1];

      setDragPoint([x, y]);
      onDragMove && onDragMove(x, y);
    };

    const touchEventsAttached = isTouchSupported();

    if (startPoint instanceof Array) {
      window.addEventListener('mouseup', handleMouseUp);
      window.addEventListener('mousemove', handleMouseMove);
      touchEventsAttached && window.addEventListener('touchend', handleMouseUp);
      touchEventsAttached &&
        window.addEventListener('touchmove', handleMouseMove);
    }

    return () => {
      window.removeEventListener('mouseup', handleMouseUp);
      window.removeEventListener('mousemove', handleMouseMove);
      touchEventsAttached &&
        window.removeEventListener('touchend', handleMouseUp);
      touchEventsAttached &&
        window.removeEventListener('touchmove', handleMouseMove);
    };
  }, [
    dragPoint,
    onDragEnd,
    onDragMove,
    setDragPoint,
    setStartPoint,
    startPoint,
  ]);

  return [dragPoint, startPoint];
};

export default useDrag;
