import { useState, useEffect, useMemo } from 'react';

const useFocus = (ref) => {
  const [state, setState] = useState(false);

  useEffect(() => {
    const handleFocus = () => setState(true);
    const handleBlur = () => setState(false);
    const refValue = ref.current;

    refValue.addEventListener('focus', handleFocus);
    refValue.addEventListener('blur', handleBlur);

    return () => {
      refValue.removeEventListener('focus', handleFocus);
      refValue.removeEventListener('blur', handleBlur);
    };
  }, [ref]);

  const setFocused = useMemo(
    () => (value) => (value ? ref.current.focus() : ref.current.blur()),
    [ref],
  );

  return [state, setFocused];
};

export default useFocus;
