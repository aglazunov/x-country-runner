import { useEffect } from 'react';

const listeners = [];
const randomId = () =>
  Math.floor(Math.random() * Number.MAX_SAFE_INTEGER).toString(16);

const keyMatches = (key, keyRule) => {
  if (keyRule instanceof Array) {
    return keyRule.includes(key);
  }

  return key === keyRule;
};

const handleKeyDown = (event) => {
  Object.values(listeners).forEach(({ ref, key, handler }) => {
    if (document.activeElement === ref.current && keyMatches(event.key, key)) {
      handler(event);
    }
  });
};

window.addEventListener('keydown', handleKeyDown);

const useKeyDown = (ref, key, handler) => {
  useEffect(() => {
    const id = randomId();
    if (!handler) {
      delete listeners[id];
    } else {
      listeners[id] = {
        ref,
        key,
        handler,
      };
    }

    return () => {
      delete listeners[id];
    };
  }, [handler, key, ref]);
};

export default useKeyDown;
