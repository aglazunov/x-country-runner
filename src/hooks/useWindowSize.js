import { useEffect, useState } from 'react';

const useWindowSize = () => {
  const [size, setSize] = useState(null);

  useEffect((onResize) => {
    const handleResize = () => {
      setSize([window.innerWidth, window.innerHeight]);
      onResize && onResize();
    };

    handleResize();

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return size;
};

export default useWindowSize;
