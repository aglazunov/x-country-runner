// https://stackoverflow.com/questions/14439903/how-can-i-detect-device-touch-support-in-javascript/34146257
export const isTouchSupported = () =>
  'ontouchstart' in window ||
  (window.DocumentTouch && document instanceof window.DocumentTouch) ||
  navigator.maxTouchPoints > 0 ||
  window.navigator.msMaxTouchPoints > 0;
