export const marker = (x, y) => ({
  x,
  y,
  displayX: x,
  displayY: y,
});

export const line = (start, end) => ({
  start,
  end,
});

const A = [-10, -15];
const B = [45, 20];
const C = [190, 10];
const D = [128, 400];
const E = [100, -200];
const F = [25, 75];

const testData = [
  {
    markers: [],
    lines: [],
  },
  {
    markers: [A],
    lines: [],
  },
  {
    markers: [A, A, A, A],
    lines: [],
  },
  {
    markers: [A, B],
    lines: [[A, B]],
  },
  {
    markers: [A, A, B, B, A],
    lines: [[A, B], [B, A]],
  },
  {
    markers: [A, B, C, D, E, A, C, B, A, D, E, A, F],
    lines: [
      [A, B],
      [B, C],
      [C, D],
      [D, E],
      [E, A],
      [A, C],
      [C, B],
      [B, A],
      [A, D],
      [D, E],
      [E, A],
      [A, F],
    ],
  },
].map(({ markers, lines }) => ({
  markers: markers.map(([...coords]) => marker(...coords)),
  lines: lines.map(([...points]) => line(...points)),
}));

export default testData;
