export const activateMarker = (id) => ({
  type: 'ACTIVATE_MARKER',
  id,
});
