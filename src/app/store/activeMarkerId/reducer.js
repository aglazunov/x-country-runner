import { createReducer } from '../util';

export default createReducer(
  {
    ACTIVATE_MARKER: (state, { id }) => id,
    INSERT_MIDDLE_MARKER: (state, { id }) => id,
    ADD_MARKER: (state, { id }) => id,
  },
  null,
);
