// ['A', 'B', 1, 0];
// [];

export const reorderArray = (array, srcIndex, targetIndex) => {
  if (
    typeof srcIndex !== 'number' ||
    typeof targetIndex !== 'number' ||
    srcIndex === targetIndex
  ) {
    return array;
  }

  return array.map((item, index) => {
    let valueIndex = index;

    if (srcIndex < targetIndex) {
      if (index >= srcIndex && index < targetIndex - 1) {
        valueIndex += 1;
      }
      if (index === targetIndex - 1) {
        valueIndex = srcIndex;
      }
    } else if (srcIndex > targetIndex) {
      if (index > targetIndex && index <= srcIndex) {
        valueIndex -= 1;
      }
      if (index === targetIndex) {
        valueIndex = srcIndex;
      }
    }

    return array[valueIndex];
  });
};
