import { reorderArray } from '../utils';

it('reorders the array item to index', () => {
  const testData = [
    { array: ['A', 'B'], src: 0, target: 0, result: ['A', 'B'] },
    { array: ['A', 'B'], src: 1, target: 1, result: ['A', 'B'] },
    { array: ['A', 'B'], src: 0, target: 1, result: ['A', 'B'] },
    { array: ['A', 'B'], src: 1, target: 0, result: ['B', 'A'] },

    { array: ['D', 'E', 'F'], src: 0, target: 3, result: ['E', 'F', 'D'] },
    {
      array: ['D', 'E', 'F', 'G'],
      src: 3,
      target: 0,
      result: ['G', 'D', 'E', 'F'],
    },

    {
      array: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'],
      src: 6,
      target: 3,
      result: ['A', 'B', 'C', 'G', 'D', 'E', 'F', 'H', 'I'],
    },
    {
      array: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'],
      src: 3,
      target: 6,
      result: ['A', 'B', 'C', 'E', 'F', 'D', 'G', 'H', 'I'],
    },
  ];

  testData.forEach(({ array, src, target, result }) => {
    expect(reorderArray(array, src, target)).toEqual(result);
  });
});
