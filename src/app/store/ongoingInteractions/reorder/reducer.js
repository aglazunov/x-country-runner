import { createReducer } from '../../util';

const initialState = {
  srcIndex: undefined,
  targetIndex: undefined,
};

const reducer = createReducer(
  {
    START_REORDER: (state, { srcIndex }) => ({
      srcIndex,
      targetIndex: undefined,
    }),
    REORDER_MOVE: (state, { targetIndex }) => ({
      ...state,
      targetIndex,
    }),
    REORDER_MARKER: () => initialState,
  },
  initialState,
);

export default reducer;
