export const startReorder = (srcIndex) => ({
  type: 'START_REORDER',
  srcIndex,
});

export const reorderMove = (targetIndex) => ({
  type: 'REORDER_MOVE',
  targetIndex,
});
