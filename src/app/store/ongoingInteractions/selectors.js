import {
  INTERACTION_STATE_DRAGGING,
  INTERACTION_STATE_REORDERING,
  INTERACTION_STATE_FREE,
} from '../constants';

export const getInteractionState = ({ move, reorder }) => {
  if (typeof move.id === 'string') {
    return INTERACTION_STATE_DRAGGING;
  }

  if (typeof reorder.srcIndex === 'number') {
    return INTERACTION_STATE_REORDERING;
  }

  return INTERACTION_STATE_FREE;
};
