export const startProvisionalMove = (id, source) => ({
  type: 'START_PROVISIONAL_MOVE',
  id,
  source,
});

export const provisionallyMoveMarker = (id, x, y, source) => ({
  type: 'PROVISIONALLY_MOVE_MARKER',
  id,
  x,
  y,
  source,
});
