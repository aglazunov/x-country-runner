import { createReducer } from '../../util';

const initialState = {
  id: undefined,
  point: undefined,
};

const reducer = createReducer(
  {
    START_PROVISIONAL_MOVE: (state, { id }) => ({
      id,
      point: [0, 0],
    }),
    PROVISIONALLY_MOVE_MARKER: (state, { id, x, y }) => ({
      id,
      point: [x, y],
    }),
    MOVE_MARKER: () => initialState,
    DELETE_MARKER: (state, { id }) => {
      if (state.id === id) {
        return initialState;
      }

      return state;
    },
  },
  initialState,
);

export default reducer;
