import { combineReducers } from '../util';

import reorderReducer from './reorder/reducer';
import moveReducer from './move/reducer';

const reducer = combineReducers({
  reorder: reorderReducer,
  move: moveReducer,
});

export default reducer;
