export const INTERACTION_STATE_DRAGGING = 'dragging';
export const INTERACTION_STATE_REORDERING = 'reordering';
export const INTERACTION_STATE_FREE = 'free';
