import testData, { line } from '../__fixtures__/markers.fixture';
import { getLines } from '../markers/selectors';

it('Calculates lines between markers', () => {
  testData.forEach(({ markers, lines }) => {
    const resultingLines = getLines(markers);
    const lineMapper = ({ start, end }) => line(start, end);

    expect(resultingLines.map(lineMapper)).toEqual(lines);
  });
});
