import { combineReducers } from './util';

import markersReducer from './markers/reducer';
import ongoingInteractionsReducer from './ongoingInteractions/reducer';
import activeMarkerReducer from './activeMarkerId/reducer';
import mapLocationReducer from './mapLocation/reducer';

const reducer = combineReducers({
  markers: markersReducer,
  mapLocation: mapLocationReducer,
  ongoingInteractions: ongoingInteractionsReducer,
  activeMarkerId: activeMarkerReducer,
});

export default reducer;
