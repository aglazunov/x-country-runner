import { getMarkerLabel, getWaypointName } from './helpers';
import { reorderArray } from '../ongoingInteractions/reorder/utils';

export const getDisplayMarkers = (markers, { move, reorder }) =>
  markers.map((marker, index) => {
    const moveInProgress = move.id === marker.id;
    const reorderInProgress = reorder.srcIndex === index;

    return {
      ...marker,
      displayX: moveInProgress ? marker.x + move.point[0] : marker.x,
      displayY: moveInProgress ? marker.y + move.point[1] : marker.y,
      label: getMarkerLabel(index),
      name: getWaypointName(index),
      moving: moveInProgress || reorderInProgress,
    };
  });

export const getLines = (displayMarkers, { srcIndex, targetIndex } = {}) => {
  const reordering = typeof srcIndex === 'number';
  let orderedMarkers = displayMarkers;

  if (reordering) {
    orderedMarkers = reorderArray(displayMarkers, srcIndex, targetIndex);
  }

  return orderedMarkers
    .map((endMarker, index) => {
      if (index === 0) {
        return null;
      }
      const startMarker = orderedMarkers[index - 1];

      // don't produce a line of 0 length
      if (
        startMarker.displayX === endMarker.displayX &&
        startMarker.displayY === endMarker.displayY
      ) {
        return null;
      }

      return {
        start: [startMarker.displayX, startMarker.displayY],
        end: [endMarker.displayX, endMarker.displayY],
        startMarkerId: startMarker.id,
        endMarkerId: endMarker.id,
        id: `${startMarker.id}-${endMarker.id}`,
        moving: reordering || startMarker.moving || endMarker.moving,
      };
    })
    .filter(Boolean);
};
