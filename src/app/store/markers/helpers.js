export const randomId = () =>
  Math.floor(Math.random() * Number.MAX_SAFE_INTEGER).toString(16);

export const newMarker = (x, y, id) => ({
  x,
  y,
  id: id || randomId(),
});

const START_CHAR = 65; // A
const ALPHABET_LENGTH = 26;

const convertChar = (n) => String.fromCharCode(START_CHAR + n);

export const getMarkerLabel = (index) => {
  let result = '';

  do {
    result = convertChar(index % ALPHABET_LENGTH) + result;
    index = Math.floor(index / ALPHABET_LENGTH) - 1;
  } while (index >= 0);

  return result;
};

export const getWaypointName = (index) => `Waypoint ${getMarkerLabel(index)}`;
