import { randomId } from './helpers';

export const addMarker = (x, y) => ({
  type: 'ADD_MARKER',
  x,
  y,
  id: randomId(),
});

export const deleteMarker = (id) => ({
  type: 'DELETE_MARKER',
  id,
});

export const moveMarker = (id, x, y) => ({
  type: 'MOVE_MARKER',
  id,
  x,
  y,
});

export const reorderMarker = (srcIndex, targetIndex) => ({
  type: 'REORDER_MARKER',
  srcIndex,
  targetIndex,
});

export const insertMiddleMarker = (previousMarkerId, x, y) => ({
  type: 'INSERT_MIDDLE_MARKER',
  previousMarkerId,
  x,
  y,
  id: randomId(),
});
