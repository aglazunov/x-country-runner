import { createReducer } from '../util';
import { newMarker } from './helpers';
import { reorderArray } from '../ongoingInteractions/reorder/utils';

const reducer = createReducer(
  {
    ADD_MARKER: (markers, { x, y, id }) => [...markers, newMarker(x, y, id)],
    DELETE_MARKER: (markers, { id }) =>
      markers.filter((marker) => marker.id !== id),
    MOVE_MARKER: (markers, { x, y, id }) =>
      markers.map((marker) =>
        marker.id !== id
          ? marker
          : {
              ...marker,
              x: marker.x + x,
              y: marker.y + y,
            },
      ),
    INSERT_MIDDLE_MARKER: (markers, { previousMarkerId, x, y, id }) => {
      const previousMarkerIndex = markers.findIndex(
        (marker) => marker.id === previousMarkerId,
      );

      return [
        ...markers.slice(0, previousMarkerIndex + 1),
        newMarker(x, y, id),
        ...markers.slice(previousMarkerIndex + 1),
      ];
    },
    REORDER_MARKER: (markers, { srcIndex, targetIndex }) =>
      reorderArray(markers, srcIndex, targetIndex),
  },
  [],
);

export default reducer;
