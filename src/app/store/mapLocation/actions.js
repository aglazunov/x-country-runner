export const setMapDimensions = (width, height) => ({
  type: 'SET_MAP_DIMENSIONS',
  width,
  height,
});
