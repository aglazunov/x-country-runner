export const getFullMapLocation = (mapLocation) => ({
  ...mapLocation,
  tilesX: mapLocation.width
    ? Math.ceil(mapLocation.width / mapLocation.tileSize)
    : 1,
  tilesY: mapLocation.height
    ? Math.ceil(mapLocation.height / mapLocation.tileSize)
    : 1,
});
