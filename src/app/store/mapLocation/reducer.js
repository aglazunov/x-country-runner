import { createReducer } from '../util';

const initialState = {
  startX: 1101,
  startY: 724,
  z: 11,
  tileSize: 256,
};

export default createReducer(
  {
    SET_MAP_DIMENSIONS: (state, { width, height }) => ({
      ...state,
      width,
      height,
    }),
  },
  initialState,
);
