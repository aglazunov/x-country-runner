export const createReducer = (fnMap, initialState) => (
  state = initialState,
  payload,
) => {
  const handler = fnMap[payload.type];

  return handler ? handler(state, payload) : state;
};

export const combineReducers = (reducerMap) => (state = {}, action) => {
  let changed = false;

  const newState = Object.entries(reducerMap).reduce(
    (result, [key, reducer]) => {
      result[key] = reducer(state[key], action);
      if (state[key] !== result[key]) {
        changed = true;
      }

      return result;
    },
    {},
  );

  return changed ? newState : state;
};

export const createDispatcherMap = (actionCreators, dispatch) =>
  Object.entries(actionCreators).reduce((result, [key, actionCreator]) => {
    result[key] = (...params) => dispatch(actionCreator(...params));

    return result;
  }, {});
