import React, { useReducer, useMemo } from 'react';

import MapContext from './MapContext';
import reducer from './store/reducer';
import * as actionCreators from './store/actions';
import AppLayout from '../components/AppLayout';
import { createDispatcherMap } from './store/util';
import { getInteractionState } from './store/ongoingInteractions/selectors';
import { getDisplayMarkers, getLines } from './store/markers/selectors';
import { getFullMapLocation } from './store/mapLocation/selectors';

const initialState = reducer(undefined, {});

const App = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { markers, ongoingInteractions, activeMarkerId, mapLocation } = state;
  const { reorder } = ongoingInteractions;

  const actions = useMemo(
    () => createDispatcherMap(actionCreators, dispatch),
    [],
  );

  const displayMarkers = useMemo(
    () => getDisplayMarkers(markers, ongoingInteractions),
    [markers, ongoingInteractions],
  );

  const lines = useMemo(() => getLines(displayMarkers, reorder), [
    displayMarkers,
    reorder,
  ]);

  const interactionState = useMemo(
    () => getInteractionState(ongoingInteractions),
    [ongoingInteractions],
  );

  const fullMapLocation = useMemo(() => getFullMapLocation(mapLocation), [
    mapLocation,
  ]);

  const contextValue = {
    markers: displayMarkers,
    mapLocation: fullMapLocation,
    lines,
    interactionState,
    ongoingInteractions,
    activeMarkerId,
    ...actions,
  };

  return (
    <MapContext.Provider value={contextValue}>
      <AppLayout onMapResize={actions.setMapDimensions} />
    </MapContext.Provider>
  );
};

export default App;
