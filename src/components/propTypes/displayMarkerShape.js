import T from 'prop-types';

export default T.shape({
  x: T.number.isRequired,
  y: T.number.isRequired,
  displayX: T.number.isRequired,
  displayY: T.number.isRequired,
  moving: T.bool.isRequired,
  label: T.string.isRequired,
  name: T.string.isRequired,
});
