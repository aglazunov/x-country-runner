import T from 'prop-types';

export default T.shape({
  id: T.string.isRequired,
  start: T.arrayOf(T.number).isRequired,
  end: T.arrayOf(T.number).isRequired,
});
