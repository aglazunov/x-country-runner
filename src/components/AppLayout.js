import React, { useRef, useLayoutEffect, useState } from 'react';
import T from 'prop-types';

import * as styles from './app.module.css';
import PlanningMap from './map/PlanningMap/PlanningMap';
import ManagementPanel from './panel/ManagementPanel';
import useWindowSize from '../hooks/useWindowSize';

const AppLayout = ({ onMapResize }) => {
  const mapRef = useRef(null);
  const windowSize = useWindowSize();
  const [size, setSize] = useState(null);

  const style = {};

  if (windowSize instanceof Array) {
    style.height = `${windowSize[1]}px`;
  }

  useLayoutEffect(() => {
    if (mapRef.current) {
      const newSize = [mapRef.current.offsetWidth, mapRef.current.offsetHeight];
      setSize(newSize);
      onMapResize(...newSize);
    }
  }, [onMapResize, windowSize]);

  return (
    <div className={styles.app} style={style}>
      <div className={styles.leftPanel}>
        <ManagementPanel />
      </div>
      <div className={styles.mapContent} ref={mapRef}>
        <PlanningMap size={size} />
      </div>
    </div>
  );
};

AppLayout.propTypes = {
  onMapResize: T.func.isRequired,
};

export default AppLayout;
