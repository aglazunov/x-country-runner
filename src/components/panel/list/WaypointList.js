import React, { useCallback, memo, useState, useEffect } from 'react';
import T from 'prop-types';

import WaypointItem from './waypoint/WaypointItem';
import * as styles from './waypoint/waypoint.module.css';
import displayMarkerShape from '../../propTypes/displayMarkerShape';
import {
  INTERACTION_STATE_FREE,
  INTERACTION_STATE_REORDERING,
} from '../../../app/store/constants';
import { isTouchSupported } from '../../../hooks/helpers';

const WaypointList = ({
  markers,
  startReorder,
  reorderMarker,
  reorder,
  interactionState,
  reorderMove,
}) => {
  const reordering = interactionState === INTERACTION_STATE_REORDERING;
  const inFreeState = interactionState === INTERACTION_STATE_FREE;
  const [insertIndex, setInsertIndex] = useState(null);

  const finalizeReorder = () => {
    reorderMarker(reorder.srcIndex, reorder.targetIndex);
  };

  const listClass = [styles.list, reordering ? styles.reordering : undefined]
    .filter(Boolean)
    .join(' ');

  const handleRollOut = useCallback(() => {
    if (reordering) {
      setInsertIndex(null);
    }
  }, [reordering]);

  const handleRollOver = useCallback(
    (index) => {
      if (reordering) {
        setInsertIndex(index);
      }
    },
    [reordering],
  );

  useEffect(() => {
    reorderMove(insertIndex);
  }, [insertIndex, reorderMove]);

  if (markers.length === 0) {
    return (
      <div>
        Add waypoints by {isTouchSupported() ? 'tapping' : 'clicking'} on the map
      </div>
    );
  }

  return (
    <div className={listClass}>
      {markers.map(({ label, id }, index) => (
        <WaypointItem
          key={id}
          label={`Waypoint ${label}`}
          id={id}
          insertBefore={index === insertIndex}
          insertAfter={index + 1 === insertIndex}
          index={index}
          startReorder={startReorder}
          finalizeReorder={finalizeReorder}
          showDragHandle={!reordering && markers.length > 1}
          showDeleteButton={!reordering}
          onRollOut={handleRollOut}
          onRollOver={handleRollOver}
          interactionsEnabled={inFreeState}
        />
      ))}
    </div>
  );
};

WaypointList.propTypes = {
  markers: T.arrayOf(displayMarkerShape).isRequired,
  startReorder: T.func.isRequired,
  reorderMove: T.func.isRequired,
  reorderMarker: T.func.isRequired,
  // eslint-disable-next-line react/require-default-props
  reorder: T.shape({
    srcIndex: T.number,
    targetIndex: T.number,
  }),
  interactionState: T.string.isRequired,
};

export default memo(WaypointList);
