import React from 'react';

import MapContext from '../../../app/MapContext';
import WaypointList from './WaypointList';

const ConnectedWaypointList = () => (
  <MapContext.Consumer>
    {({
      markers,
      startReorder,
      reorderMove,
      interactionState,
      reorderMarker,
      ongoingInteractions: { reorder },
    }) => (
      <WaypointList
        interactionState={interactionState}
        markers={markers}
        startReorder={startReorder}
        reorderMove={reorderMove}
        reorderMarker={reorderMarker}
        reorder={reorder}
      />
    )}
  </MapContext.Consumer>
);

export default ConnectedWaypointList;
