import React, { useRef, useCallback, memo } from 'react';
import T from 'prop-types';

import * as styles from './waypoint.module.css';
import { ReactComponent as DragHandle } from './drag.svg';
import useDrag from '../../../../hooks/useDrag';
import DeleteButton from './DeleteButton';

const overTopPart = (event) =>
  event.clientY - event.currentTarget.offsetTop <
  (3 * event.currentTarget.offsetHeight) / 4;

const WaypointItem = ({
  label,
  id,
  index,
  insertBefore,
  insertAfter,
  startReorder,
  finalizeReorder,
  showDragHandle,
  showDeleteButton,
  onRollOver,
  onRollOut,
  interactionsEnabled,
}) => {
  const onDragStart = useCallback(() => startReorder(index), [
    index,
    startReorder,
  ]);
  const onDragEnd = useCallback(() => finalizeReorder(), [finalizeReorder]);

  const ref = useRef(null);
  const [dragPoint, dragStartPoint] = useDrag(
    ref,
    onDragStart,
    undefined,
    onDragEnd,
  );
  const dragging = dragPoint instanceof Array;

  const handleRollOver = useCallback(
    (event) => {
      onRollOver(overTopPart(event) ? index : index + 1);
    },
    [index, onRollOver],
  );
  const handleRollOut = useCallback(() => onRollOut(index), [index, onRollOut]);

  const waypointClass = [
    styles.waypoint,
    dragging ? styles.dragged : undefined,
    showDragHandle ? styles.showDragHandle : undefined,
    showDeleteButton ? styles.showDeleteButton : undefined,
    insertBefore ? styles.insertBefore : undefined,
    insertAfter ? styles.insertAfter : undefined,
  ]
    .filter(Boolean)
    .join(' ');

  let style;
  if (dragging && dragStartPoint instanceof Array) {
    style = {
      right: 0,
      top: `${dragStartPoint[1] + dragPoint[1]}px`,
    };
  }

  const handlers = {};

  if (!dragging) {
    handlers.onMouseOver = handleRollOver;
    handlers.onMouseMove = handleRollOver;
    handlers.onMouseOut = handleRollOut;
  }

  return (
    // eslint-disable-next-line jsx-a11y/mouse-events-have-key-events
    <div className={waypointClass} {...handlers} style={style}>
      <div>
        <DragHandle
          ref={ref}
          className={styles.dragIcon}
          disabled={!interactionsEnabled}
        />
        {label}
      </div>
      <DeleteButton id={id} disabled={!interactionsEnabled} />
    </div>
  );
};

WaypointItem.propTypes = {
  label: T.string.isRequired,
  id: T.string.isRequired,
  index: T.number.isRequired,
  startReorder: T.func.isRequired,
  finalizeReorder: T.func.isRequired,
  showDragHandle: T.bool.isRequired,
  showDeleteButton: T.bool.isRequired,
  onRollOver: T.func,
  onRollOut: T.func,
  insertBefore: T.bool,
  insertAfter: T.bool,
  interactionsEnabled: T.bool,
};

WaypointItem.defaultProps = {
  onRollOver: undefined,
  onRollOut: undefined,
  insertBefore: false,
  insertAfter: false,
  interactionsEnabled: true,
};

export default memo(WaypointItem);
