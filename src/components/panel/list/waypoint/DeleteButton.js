import React from 'react';
import T from 'prop-types';

import MapContext from '../../../../app/MapContext';
import * as styles from './waypoint.module.css';
import { ReactComponent as DeleteIcon } from './delete.svg';

const DeleteButton = ({ id, disabled }) => (
  <MapContext.Consumer>
    {({ deleteMarker }) => (
      <button
        className={styles.deleteButton}
        disabled={disabled}
        type="button"
        onClick={() => deleteMarker(id)}
      >
        <DeleteIcon className={styles.binIcon} />
      </button>
    )}
  </MapContext.Consumer>
);

DeleteButton.propTypes = {
  id: T.string.isRequired,
  disabled: T.bool,
};

DeleteButton.defaultProps = {
  disabled: false,
};

export default DeleteButton;
