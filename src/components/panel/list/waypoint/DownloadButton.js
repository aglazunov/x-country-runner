import React from 'react';
import T from 'prop-types';

import * as styles from '../../panel.module.css';

const DownloadButton = ({ download, ...props }) => (
  <button
    type="button"
    className={styles.actionButton}
    onClick={download}
    {...props}
  >
    Download GPX
  </button>
);

DownloadButton.propTypes = {
  download: T.func.isRequired,
};

export default DownloadButton;
