import React from 'react';

import * as styles from './panel.module.css';
import ConnectedWaypointList from './list/ConnectedWaypointList';
import GpxBuilder from '../export/GpxBuilder';
import DownloadButton from './list/waypoint/DownloadButton';

const ManagementPanel = () => (
  <div className={styles.panel}>
    <h3 className={styles.header}>Route Builder</h3>
    <ConnectedWaypointList />
    <div className={styles.buttonContainer}>
      <GpxBuilder triggerComponent={DownloadButton} />
    </div>
  </div>
);

export default ManagementPanel;
