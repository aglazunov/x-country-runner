const waypointXml = (tagName, lat, lng, name) =>
  `<${tagName} lat="${lat.toFixed(6)}" lon="${lng.toFixed(6)}">
        <name>${name}</name>
    </${tagName}>`;

const getTemplateString = (markers) => {
  const waypointsSection = markers
    .map(({ lat, lng, name }) => waypointXml('wpt', lat, lng, name))
    .join('\n    ');

  const routePointsSection = markers
    .map(({ lat, lng, name }) =>
      waypointXml('rtept', lat, lng, name)
        .split('\n')
        .join('\n    '),
    )
    .join('\n        ');

  return `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gpx version="1.1" creator="xRunner - xrunner.glazunov.eu" xmlns="http://www.topografix.com/GPX/1/1" 
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
     xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
    <metadata>
        <name>GPX route created with xRunner</name>
        <author>
          <link href="https://xrunner.glazunov.eu">
              <text>xRunner</text>
              <type>text/html</type>
          </link>
        </author>
        <time>${new Date().toISOString()}</time>
    </metadata>
    ${waypointsSection}
    <rte>
        <name>Cross Country Route</name>
        ${routePointsSection}        
    </rte>
</gpx>`;
};

export const getGpxMarkup = (markers) => {
  return getTemplateString(markers);
};

// modified from https://stackoverflow.com/questions/3665115/how-to-create-a-file-in-memory-for-user-to-download-but-not-through-server
export const downloadAsFile = (text, fileName) => {
  const a = document.createElement('a');
  // eslint-disable-next-line no-multi-assign
  const url = (a.href = URL.createObjectURL(
    new Blob([text], { type: 'text' }),
  ));
  a.download = fileName;

  // Append anchor to body.
  document.body.appendChild(a);
  a.click();

  // Remove anchor from body
  document.body.removeChild(a);
  URL.revokeObjectURL(url);
};
