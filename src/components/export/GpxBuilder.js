import React from 'react';
import T from 'prop-types';

import MapContext from '../../app/MapContext';
import { downloadAsFile, getGpxMarkup } from './util';
import { tile2lat, tile2long } from '../map/MapSource/helpers';

const GpxBuilder = ({ triggerComponent: DownloadTrigger }) => {
  const handleDownload = (markers, { startX, startY, tileSize, z }) => {
    const convertMarker = ({ x, y, name }) => ({
      name,
      lng: tile2long(startX + x / tileSize, z),
      lat: tile2lat(startY + y / tileSize, z),
    });

    const markup = getGpxMarkup(markers.map(convertMarker));
    downloadAsFile(markup, 'xRunner.gpx');
  };

  return (
    <MapContext.Consumer>
      {({ markers, mapLocation }) => (
        <DownloadTrigger
          download={() => handleDownload(markers, mapLocation)}
          disabled={markers.length === 0}
          title={
            markers.length ? undefined : 'Empty run: add waypoints to download'
          }
        />
      )}
    </MapContext.Consumer>
  );
};

GpxBuilder.propTypes = {
  triggerComponent: T.elementType.isRequired,
};

export default GpxBuilder;
