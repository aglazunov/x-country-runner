import React, { memo } from 'react';
import T from 'prop-types';

import * as styles from '../map.module.css';
import ConnectedMapOverlay from '../MapOverlay/ConnectedMapOverlay';
import ConnectedMapSource from '../MapSource/ConnectedMapSource';

const PlanningMap = ({ size }) => (
  <div id="map" className={styles.planningMap}>
    <div
      className={styles.mapSource}
      style={{ width: size ? `${size[0]}px` : undefined }}
    >
      <ConnectedMapSource />
    </div>
    <div
      className={styles.mapOverlay}
      style={{ width: size ? `${size[0]}px` : undefined }}
    >
      <ConnectedMapOverlay />
    </div>
  </div>
);

PlanningMap.propTypes = {
  size: T.arrayOf(T.number),
};

PlanningMap.defaultProps = {
  size: undefined,
};

export default memo(PlanningMap);
