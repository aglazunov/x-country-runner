import { getMarkerLabel } from '../../../../app/store/markers/helpers';

it('Returns labels as an alphabet letter for first 26 markers', () => {
  expect(getMarkerLabel(0)).toEqual('A');
  expect(getMarkerLabel(1)).toEqual('B');
  expect(getMarkerLabel(2)).toEqual('C');
  expect(getMarkerLabel(3)).toEqual('D');
  expect(getMarkerLabel(4)).toEqual('E');
  expect(getMarkerLabel(5)).toEqual('F');
  expect(getMarkerLabel(24)).toEqual('Y');
  expect(getMarkerLabel(25)).toEqual('Z');
});

it('Stacks letters one after for markers with index >= 26', () => {
  expect(getMarkerLabel(26)).toEqual('AA');
  expect(getMarkerLabel(27)).toEqual('AB');
  expect(getMarkerLabel(26 + 25)).toEqual('AZ');
  expect(getMarkerLabel(26 * 2)).toEqual('BA');
  expect(getMarkerLabel(26 * 3)).toEqual('CA');
  expect(getMarkerLabel(26 * 3 + 2)).toEqual('CC');
  expect(getMarkerLabel(26 * 3 + 3)).toEqual('CD');
  expect(getMarkerLabel(26 * 3 + 25)).toEqual('CZ');
  expect(getMarkerLabel(26 * 26 + 25)).toEqual('ZZ');
  expect(getMarkerLabel(26 * 26 + 26)).toEqual('AAA');
});
