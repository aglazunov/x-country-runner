import React, { memo } from 'react';
import T from 'prop-types';

import Line from '../primitives/Line';
import lineShape from '../../../propTypes/lineShape';

const LineLayer = ({ insertMiddleMarker, lines }) => (
  <g>
    {lines.map(({ id, start, end, moving, startMarkerId }) => (
      <Line
        key={id}
        start={start}
        end={end}
        moving={moving}
        startMarkerId={startMarkerId}
        onAddMidpoint={(x, y) => insertMiddleMarker(startMarkerId, x, y)}
      />
    ))}
  </g>
);

LineLayer.propTypes = {
  lines: T.arrayOf(lineShape).isRequired,
  insertMiddleMarker: T.func.isRequired,
};

export default memo(LineLayer);
