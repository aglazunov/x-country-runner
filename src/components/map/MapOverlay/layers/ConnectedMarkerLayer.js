import React from 'react';

import MapContext from '../../../../app/MapContext';
import MarkerLayer from './MarkerLayer';

const ConnectedMarkerLayer = () => (
  <MapContext.Consumer>
    {({ ...props }) => <MarkerLayer {...props} />}
  </MapContext.Consumer>
);

export default ConnectedMarkerLayer;
