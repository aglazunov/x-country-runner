import React from 'react';
import MapContext from '../../../../app/MapContext';
import LineLayer from './LineLayer';

const ConnectedLineLayer = () => (
  <MapContext.Consumer>
    {({ insertMiddleMarker, lines }) => (
      <LineLayer insertMiddleMarker={insertMiddleMarker} lines={lines} />
    )}
  </MapContext.Consumer>
);

export default ConnectedLineLayer;
