import React, { memo } from 'react';
import T from 'prop-types';

import Marker from '../primitives/Marker';
import { INTERACTION_STATE_FREE } from '../../../../app/store/constants';
import displayMarkerShape from '../../../propTypes/displayMarkerShape';

const MarkerLayer = ({
  markers,
  interactionState,
  startProvisionalMove,
  provisionallyMoveMarker,
  moveMarker,
  deleteMarker,
  activeMarkerId,
}) => (
  <g>
    {markers.map(({ id, displayX, displayY, label, moving }) => {
      const inFreeState = interactionState === INTERACTION_STATE_FREE;
      const deleteHandler = inFreeState ? () => deleteMarker(id) : undefined;

      return (
        <Marker
          key={id}
          x={displayX}
          y={displayY}
          label={label}
          active={id === activeMarkerId}
          moving={moving}
          onDelete={deleteHandler}
          onMoveStart={() => startProvisionalMove(id)}
          onMove={(deltaX, deltaY) =>
            provisionallyMoveMarker(id, deltaX, deltaY)
          }
          onMoveEnd={(deltaX, deltaY) => moveMarker(id, deltaX, deltaY)}
        />
      );
    })}
  </g>
);

MarkerLayer.propTypes = {
  markers: T.arrayOf(displayMarkerShape).isRequired,
  interactionState: T.string.isRequired,
  startProvisionalMove: T.func.isRequired,
  provisionallyMoveMarker: T.func.isRequired,
  moveMarker: T.func.isRequired,
  deleteMarker: T.func.isRequired,
  activeMarkerId: T.string,
};

MarkerLayer.defaultProps = {
  activeMarkerId: null,
};

export default memo(MarkerLayer);
