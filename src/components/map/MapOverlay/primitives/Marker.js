import React, { useRef, memo, useEffect } from 'react';
import T from 'prop-types';

import * as styles from './marker.module.css';
import useDrag from '../../../../hooks/useDrag';
import useKeyDown from '../../../../hooks/useKeyDown';
import useFocus from '../../../../hooks/useFocus';

const Marker = ({
  x,
  y,
  label,
  onMoveStart,
  onMove,
  onMoveEnd,
  onDelete,
  moving,
  active,
}) => {
  const ref = useRef(null);
  useDrag(ref, onMoveStart, onMove, onMoveEnd);
  useKeyDown(ref, ['Delete', 'Backspace'], onDelete);
  const [, setFocused] = useFocus(ref);

  useEffect(() => {
    setFocused(active);
  }, [active, setFocused]);

  const markerClass = [styles.marker, moving && styles.moving]
    .filter(Boolean)
    .join(' ');

  return (
    <g
      transform={`translate(${x},${y})`}
      className={markerClass}
      tabIndex={0}
      ref={ref}
    >
      <circle r={15} />
      <text>{label}</text>
    </g>
  );
};

Marker.propTypes = {
  x: T.number.isRequired,
  y: T.number.isRequired,
  moving: T.bool,
  label: T.string,
  onMoveStart: T.func,
  onMove: T.func,
  onMoveEnd: T.func,
  onDelete: T.func,
  active: T.bool,
};

Marker.defaultProps = {
  label: '',
  onMoveStart: undefined,
  onMove: undefined,
  onMoveEnd: undefined,
  onDelete: undefined,
  moving: false,
  active: false,
};

export default memo(Marker);
