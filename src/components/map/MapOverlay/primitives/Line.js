import React, { memo } from 'react';
import T from 'prop-types';
import * as styles from './line.module.css';
import Midpoint from './Midpoint';

const Line = ({ start: [x1, y1], end: [x2, y2], moving, onAddMidpoint }) => {
  const coords = { x1, x2, y1, y2 };
  const [cx, cy] = [(x1 + x2) / 2, (y1 + y2) / 2];
  const showMidpoint = Boolean(!moving && onAddMidpoint);
  const handleMidpointClick = () => onAddMidpoint(cx, cy);

  return (
    <g className={moving ? styles.moving : undefined}>
      <line className={styles.outer} {...coords} />
      <line className={styles.inner} {...coords} />
      {showMidpoint && <Midpoint x={cx} y={cy} onClick={handleMidpointClick} />}
    </g>
  );
};

Line.propTypes = {
  start: T.arrayOf(T.number).isRequired,
  end: T.arrayOf(T.number).isRequired,
  moving: T.bool.isRequired,
  onAddMidpoint: T.func,
};

Line.defaultProps = {
  onAddMidpoint: undefined,
};

export default memo(Line);
