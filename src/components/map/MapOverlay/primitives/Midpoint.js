import T from 'prop-types';
import React from 'react';

import * as styles from './midpoint.module.css';

const MIDPOINT_RADIUS = 7;
const MIDPOINT_HIT_RADIUS = 12;

const Midpoint = ({ x, y, onClick }) => (
  <g className={styles.midpoint}>
    <circle
      className={styles.hit}
      cx={x}
      cy={y}
      r={MIDPOINT_HIT_RADIUS}
      onClick={onClick}
    />
    <circle
      className={styles.visual}
      cx={x}
      cy={y}
      r={MIDPOINT_RADIUS}
      onClick={onClick}
    />
  </g>
);

Midpoint.propTypes = {
  x: T.number.isRequired,
  y: T.number.isRequired,
  onClick: T.func,
};

Midpoint.defaultProps = {
  onClick: undefined,
};

export default Midpoint;
