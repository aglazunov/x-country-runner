import React, { memo } from 'react';

import MapContext from '../../../app/MapContext';
import MapOverlay from './MapOverlay';

const ConnectedMapOverlay = () => (
  <MapContext.Consumer>
    {({ addMarker }) => <MapOverlay addMarker={addMarker} />}
  </MapContext.Consumer>
);

export default memo(ConnectedMapOverlay);
