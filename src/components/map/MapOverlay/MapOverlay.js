import React from 'react';
import T from 'prop-types';

import * as styles from '../map.module.css';
import ConnectedLineLayer from './layers/ConnectedLineLayer';
import ConnectedMarkerLayer from './layers/ConnectedMarkerLayer';

const MapOverlay = ({ addMarker }) => {
  const handleAddMarker = (event) => {
    if (event.currentTarget === event.target) {
      const { x, y } = event.target.getBoundingClientRect();
      addMarker(event.clientX - x, event.clientY - y);
    }
  };

  return (
    <svg className={styles.mapOverlay} onClick={handleAddMarker}>
      <ConnectedLineLayer />
      <ConnectedMarkerLayer />
    </svg>
  );
};

MapOverlay.propTypes = {
  addMarker: T.func.isRequired,
};

export default MapOverlay;
