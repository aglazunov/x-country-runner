import React from 'react';
import T from 'prop-types';
import { getTileUrl } from './helpers';

const Tile = ({ x, y, z, left, top, tileSize }) => {
  const tileUrl = getTileUrl(x, y, z);

  return (
    <img
      alt={`Map tile ${left} - ${top}`}
      height={tileSize}
      src={tileUrl}
      style={{
        position: 'absolute',
        left: `${left}px`,
        top: `${top}px`,
      }}
      width={tileSize}
    />
  );
};

Tile.propTypes = {
  x: T.number.isRequired,
  y: T.number.isRequired,
  z: T.number.isRequired,
  tileSize: T.number.isRequired,
  left: T.number.isRequired,
  top: T.number.isRequired,
};

export default Tile;
