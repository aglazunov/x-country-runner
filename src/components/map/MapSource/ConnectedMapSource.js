import React from 'react';

import MapContext from '../../../app/MapContext';
import MapSource from './MapSource';

export default () => (
  <MapContext.Consumer>
    {({ mapLocation }) => <MapSource {...mapLocation} />}
  </MapContext.Consumer>
);
