const prefix = 'https://b.tile.opentopomap.org';
const postfix = '.png';

export const getTileUrl = (x, y, z, density) => {
  const densityFactor = typeof density === 'number' ? `@${density}x` : '';

  return `${prefix}/${z}/${x}/${y}${densityFactor}${postfix}`;
};

// https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#ECMAScript_.28JavaScript.2FActionScript.2C_etc..29
export const tile2long = (x, z) => (x / 2 ** z) * 360 - 180;

export const tile2lat = (y, z) => {
  const n = Math.PI - (2 * Math.PI * y) / 2 ** z;

  return (180 / Math.PI) * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n)));
};
