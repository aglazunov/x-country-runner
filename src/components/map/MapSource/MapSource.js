import React, { memo } from 'react';
import T from 'prop-types';

import Tile from './Tile';

const MapSource = ({ startX, startY, z, tilesX, tilesY, tileSize }) => {
  const tileCoordinates = Array.from(new Array(tilesX * tilesY), (item, i) => ({
    x: Math.floor(i % tilesX),
    y: Math.floor(i / tilesX),
  }));

  return (
    <>
      {tileCoordinates.map(({ x, y }, index) => (
        <Tile
          key={String(index)}
          x={startX + x}
          y={startY + y}
          z={z}
          tileSize={tileSize}
          left={x * tileSize}
          top={y * tileSize}
        />
      ))}
    </>
  );
};

MapSource.propTypes = {
  startX: T.number.isRequired,
  startY: T.number.isRequired,
  z: T.number.isRequired,
  tilesX: T.number.isRequired,
  tilesY: T.number.isRequired,
  tileSize: T.number.isRequired,
};

export default memo(MapSource);
