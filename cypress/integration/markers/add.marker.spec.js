import { generateSpiralPositions } from '../../fixtures/generate';

describe('Adding markers', () => {
  it('Adds new markers and checks their positions', () => {
    cy.visit('/');

    const leftPanelAddition = 200;
    const [width, height] = [800, 800];
    cy.viewport(width + leftPanelAddition, height);

    const step = 200;
    const positions = generateSpiralPositions(
      [step, step, width - step, height - step],
      step,
    );

    positions.forEach(([x, y]) => {
      const markerSelector = `g[transform="translate(${x},${y})"]`;

      cy.get(markerSelector).should('not.exist');
      cy.get('#map svg').click(x, y);
      cy.get(markerSelector).should('exist');
    });
  });

  it('Adds new markers and checks their titles', () => {
    cy.visit('/');

    const [width, height] = [1000, 800];
    cy.viewport(width, height);

    const step = 100;
    const x0 = width / 2;
    const y0 = height / 2;
    const data = [
      { position: [x0, y0], label: 'A' },
      { position: [x0 - step, y0 - step], label: 'B' },
      { position: [x0 + step, y0 - step], label: 'C' },
      { position: [x0 + step, y0 + step], label: 'D' },
      { position: [x0 - step, y0 + step], label: 'E' },
      { position: [x0 - step * 2, y0], label: 'F' },
    ];

    data.forEach(({ position: [x, y], label }) => {
      const markerSelector = `g > text`;

      cy.contains(markerSelector, label).should('not.exist');
      cy.get('#map svg').click(x, y);
      cy.contains(markerSelector, label).should('exist');
    });
  });
});
