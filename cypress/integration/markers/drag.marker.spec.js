import { generateSpiralPositions } from '../../fixtures/generate';

describe('Dragging markers', () => {
  it('Drags the markers to new positions', () => {
    cy.visit('/');

    const leftPanelAddition = 200;
    const [width, height] = [800, 800];
    cy.viewport(width + leftPanelAddition, height);

    const step = 150;
    const bounds = [step, step, width - step, height - step];

    const originalPositions = generateSpiralPositions(bounds, step);
    const newPositions = generateSpiralPositions(
      bounds.map((coord) => coord - step / 2),
      step,
      false,
    );

    // adding all markers
    originalPositions.forEach(([x, y]) => {
      cy.get('#map svg').click(x, y);
    });

    originalPositions.forEach(([originalX, originalY], index) => {
      const [newX, newY] = newPositions[index];
      const originalMarkerSelector = `g[transform="translate(${originalX},${originalY})"]`;
      const newMarkerSelector = `g[transform="translate(${newX},${newY})"]`;

      cy.get(originalMarkerSelector).trigger('mousedown', { which: 1 });
      cy.get('#map svg')
        .trigger('mousemove', newX, newY)
        .trigger('mouseup');

      cy.get(originalMarkerSelector).should('not.exist');
      cy.get(newMarkerSelector).should('exist');
    });
  });
});
