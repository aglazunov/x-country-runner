describe('Deleting markers', () => {
  it('Delete/Add cycle', () => {
    cy.visit('/');

    const leftPanelAddition = 200;
    const [width, height] = [800, 800];
    cy.viewport(width + leftPanelAddition, height);

    const step = 100;
    const x0 = width / 2;
    const y0 = height / 2;
    const positions = [
      [x0, y0],
      [x0 - step, y0 - step],
      [x0 + step, y0 - step],
      [x0 + step, y0 + step],
      [x0 - step, y0 + step],
      [x0 - 2 * step, y0],
    ];

    const checkLinesAndMarkersCount = (markerCount) => {
      const linesContainer = cy.get('#map svg > g:nth-child(1)');
      linesContainer
        .children()
        .should('have.length', markerCount < 2 ? 0 : markerCount - 1);

      const markersContainer = cy.get('#map svg > g:nth-child(2)');
      markersContainer.children().should('have.length', markerCount);
    };

    const addAllMarkers = () => {
      let count = 0;
      positions.forEach(([x, y]) => {
        cy.get('#map svg').click(x, y);
        count += 1;
        checkLinesAndMarkersCount(count);
      });
    };

    const deleteAllMarkers = () => {
      let count = positions.length;

      while (count > 0) {
        checkLinesAndMarkersCount(count);
        cy.contains('#map svg text', 'A').should('exist');
        cy.contains('#map svg text', 'A')
          .parent()
          .focus()
          .type('{del}');
        count -= 1;
      }

      cy.contains('#map svg text', 'A').should('not.exist');

      checkLinesAndMarkersCount(0);
    };

    addAllMarkers();
    deleteAllMarkers();
    addAllMarkers();
    deleteAllMarkers();
    addAllMarkers();
  });
});
