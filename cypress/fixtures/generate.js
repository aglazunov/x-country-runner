export const generateSpiralPositions = (
  originalBounds,
  step,
  clockwise = true,
) => {
  const bounds = [...originalBounds];
  const positions = [];
  let point = [bounds[0], bounds[1]];
  let directionIndex = 0;

  positions.push(point);

  let changes = 0;

  const directions = clockwise
    ? [[1, 0], [0, 1], [-1, 0], [0, -1]]
    : [[0, 1], [1, 0], [0, -1], [-1, 0]];
  // if we rotated around and haven't found exit, the spiral has ended
  while (changes <= 4 && bounds[2] >= bounds[0] && bounds[3] >= bounds[1]) {
    let direction = directions[directionIndex];
    const x = point[0] + direction[0] * step;
    const y = point[1] + direction[1] * step;

    if (x >= bounds[0] && y >= bounds[1] && x <= bounds[2] && y <= bounds[3]) {
      point = [x, y];
      positions.push(point);
      changes = 0;
      // if (positions.some((position) => position[0] === x && position[1] === y)) {
    } else {
      directionIndex = (directionIndex + 1) % directions.length;
      direction = directions[directionIndex];
      changes += 1;

      // if we're turning away, that means the bound is getting narrower
      direction[0] > 0 && (bounds[0] += step);
      direction[0] < 0 && (bounds[2] -= step);
      direction[1] > 0 && (bounds[1] += step);
      direction[1] < 0 && (bounds[3] -= step);
    }
  }

  return positions;
};
